We will be building an Uninterruptiple Power Supply microHAT that will be attached to a Raspberry
Pi Zero W board.The main uses of our UPS will be to act as a back-up power source when the main power 
source is unavailable, it can also act as a protective measure to prevent the Raspberry Pi Zero W from suddenly 
shutting down during power surges and lastly it can be used as a powerbank for Raspberry Pi Zero W board within 
portable devices like cameras or mobile routers

How to use: 
1. Make sure UPS is charged   
2. Attach UPS 40 GPIO pin to Raspberry Pi Zero W board
